package org.stepdefinition;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDef {
	public static WebDriver driver;

	@Given("user has to launch the web application demowebshop.tricentis.com through browser")
	public void user_has_to_launch_the_web_application_demowebshop_tricentis_com_through_browser() {
		driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));

	}

	@When("user click the login button")
	public void user_click_the_login_button() {
		driver.findElement(By.linkText("Log in")).click();

	}

	@When("user has to Enter the valid username and valild password")
	public void user_has_to_enter_the_valid_username_and_valild_password() {
		driver.findElement(By.id("Email")).sendKeys("jerilreegan@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Jeril@2807");

	}

	@When("user has to click the Login button")
	public void user_has_to_click_the_login_button() {
		driver.findElement(By.xpath("(//input[@type='submit'])[2]")).click();

	}

	@Then("user should navigate to shopping page")
	public void user_should_navigate_to_shopping_page() {
		System.out.println(driver.findElement(By.xpath("//div[@class='topic-html-content']")).getText());

	}

	@Given("user has to login into the application")
	public void user_has_to_login_into_the_application() {
		System.out.println(driver.findElement(By.xpath("//div[@class='topic-html-content']")).getText());

	}

	@When("user click the Books")
	public void user_click_the_books() {
		driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Books')]")).click();

	}

	@When("click the Sortby option")
	public void click_the_sortby_option() {
		driver.findElement(By.id("products-orderby")).click();

	}

	@When("select High to low Option in dropdown")
	public void select_high_to_low_option_in_dropdown() {
		WebElement option = driver.findElement(By.id("products-orderby"));
		Select s = new Select(option);
		s.selectByVisibleText("Price: High to Low");

	}

	@When("click Add to cart button")
	public void click_add_to_cart_button1() throws InterruptedException {
		driver.findElement(By.xpath("(//input[@class='button-2 product-box-add-to-cart-button'])[2]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("(//input[@class='button-2 product-box-add-to-cart-button'])[3]")).click();

	}

	@Then("product has been Added to Cart")
	public void product_has_been_added_to_cart() {
		System.out.println("Product has been added Successfully");

	}

	@When("user move to Electronics")
	public void user_move_to_electronics() throws InterruptedException {
		Thread.sleep(2000);
		WebElement elc = driver.findElement(By.xpath("(//a[contains(text(),'Electronics')])[1]"));
		Actions a = new Actions(driver);
		a.moveToElement(elc).build().perform();
	  
		
	}

	@When("user has to select Cellphones from dropdown")
	public void user_has_to_select_cellphones_from_dropdown() {
		driver.findElement(By.xpath("(//a[contains(text(),'Cell phones')])[1]")).click();
		

	}

	@When("select the product")
	public void select_the_product() {

		driver.findElement(By.xpath("//div[@class='product-item']//img[@title='Show details for Smartphone']")).click();

	}

	@When("click Add to Cart button")
	public void click_add_to_cart_button() {
		driver.findElement(By.id("add-to-cart-button-43")).click();

	}

	@Then("return the count of Items that added in our Cart")
	public void return_the_count_of_items_that_added_in_our_cart() {
		System.out.println("Total Number of Product in Cart"
				+ driver.findElement(By.xpath("//span[@class='cart-qty']")).getText());

	}

	@When("user click the GiftCards")
	public void user_click_the_gift_cards() {
		driver.findElement(By.xpath("(//a[contains(text(),'Gift Cards')])[1]")).click();

	}

	@When("user has to Display options")
	public void user_has_to_display_options() {
		driver.findElement(By.id("products-pagesize")).click();

	}

	@When("select option four per page from dropdown")
	public void select_option_four_per_page_from_dropdown() {
		WebElement size = driver.findElement(By.id("products-pagesize"));
		Select s1 = new Select(size);
		s1.selectByVisibleText("4");

	}

	@Then("return any giftcard name and price")
	public void return_any_giftcard_name_and_price() {
		System.out.println(driver.findElement(By.xpath("//a[text()='$5 Virtual Gift Card']")).getText());
		System.out.println(driver.findElement(By.xpath("//span[text()='5.00']")).getText());

	}

	@When("user click the logout")
	public void user_click_the_logout() {
		driver.findElement(By.xpath("(//a[text()='Log out'])[1]")).click();

	}

	@Then("user should navigate to homepage check if login is getting displayed")
	public void user_should_navigate_to_homepage_check_if_login_is_getting_displayed() {
		WebElement log = driver.findElement(By.xpath("(//a[text()='Log in'])[1]"));
		System.out.println(log.isDisplayed());
		driver.close();

	}

}
