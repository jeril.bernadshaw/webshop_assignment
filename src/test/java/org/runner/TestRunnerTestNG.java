package org.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/resources/Feature", glue = "org.stepDef")

public class TestRunnerTestNG extends AbstractTestNGCucumberTests{

}
