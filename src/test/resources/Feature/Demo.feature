Feature: Validate the Login functionality of DemoWebshop Web Application

  Scenario: Validate the Login functionality with Valid username and password
    Given user have to launch the web application demowebshop.tricentis.com through browser
    When user click the login button
    And user has to Enter the valid username and valild password
    And user has to click the Login button
    Then user should navigate to shopping page

  Scenario: Validate the Book Functionality
    Given user have to login into the application
    When user click the Books
    And click the Sortby option
    And select High to low Option in dropdown
    And click Add to cart button
    Then product has been Added to Cart

  Scenario: Validate the Electronic Functionality
   
    When user have to move to Electronics
    And user have to select Cellphones from dropdown
    And select the product
    And click Add to Cart button
    Then return the count of Items that added in our Cart

  Scenario: Validate the GiftCards Functionality
  
    When user have to click the GiftCards
    And user have to Display options
    And select option four per page from dropdown
    Then return any giftcard name and price

  Scenario: Validate the Logout Functionality
 
    When user click the logout
    Then user have display login